package com.management.library.services;

import com.management.library.Enums.CardStatus;
import com.management.library.models.Card;
import com.management.library.models.Student;
import com.management.library.DTOs.StudentDTO;
import com.management.library.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public void addStudent(Student student) {
        Card card = new Card();
        card.setStudent(student);
        card.setCardStatus(CardStatus.ACTIVE);
        student.setCard(card);
        studentRepository.save(student);
    }

    public StudentDTO findStudentById(int id) {
        Student student = studentRepository.findById(id).get();
        return new StudentDTO(id, student.getEmail(), student.getCard().getCardStatus());
    }

    public Student findStudentByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

}
