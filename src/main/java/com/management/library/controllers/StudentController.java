package com.management.library.controllers;

import com.management.library.models.Student;
import com.management.library.DTOs.StudentDTO;
import com.management.library.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("/add")
    public ResponseEntity<String> addStudent(@RequestBody Student student) {
        studentService.addStudent(student);
        return new ResponseEntity<>("Student added!", HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> getStudent(@PathVariable("id") int id) {
        StudentDTO student = studentService.findStudentById(id);
        return new ResponseEntity<>(student, HttpStatus.FOUND);
    }

    @GetMapping("/find")
    public ResponseEntity<Student> getStudentByEmail(@RequestParam("email") String email) {
        Student student = studentService.findStudentByEmail(email);
        return new ResponseEntity<>(student, HttpStatus.FOUND);
    }

}
