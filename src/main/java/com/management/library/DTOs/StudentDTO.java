package com.management.library.DTOs;
import com.management.library.Enums.CardStatus;
public class StudentDTO {
    private int id;
    private String email;
    private CardStatus status;

    public StudentDTO() {
    }

    public StudentDTO(int id, String email, CardStatus status) {
        this.id = id;
        this.email = email;
        this.status = status;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public CardStatus getStatus() {
        return status;
    }
    public void setStatus(CardStatus status) {
        this.status = status;
    }
}
