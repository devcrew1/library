package com.management.library.Enums;
public enum CardStatus {
    ACTIVE,
    INVALID,
    INACTIVE
}
